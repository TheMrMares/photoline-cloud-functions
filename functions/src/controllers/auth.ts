import firebase, { corsInstance } from '../utils/firebase'
import * as functions from 'firebase-functions'

export const doesUserExist = async (email: String): Promise<any> => {
    const res = await firebase.firestore()
        .collection('users')
        .where('email', '==', email)
        .get()
    return res.docs[0].data()
}

export const registerUser = functions.https.onRequest(
    async (req: any, res: any) => {    
        const { email, firstName, lastName, password } = req.body
        const userRes: any = await doesUserExist(email)
        if (!userRes) {
            const usersCollectionRes = await firebase.firestore()
                .collection('users')
                .add({
                    email,
                    firstName,
                    lastName,
                })
            if (usersCollectionRes) {
                const userRecord = await firebase.auth().createUser({
                    email,
                    emailVerified: true,
                    password: `${password}`,
                    displayName: `${firstName} ${lastName}`
                })
                return res.status(200).json({
                    res: userRecord,
                })
            }
        } 
        return res.send(400).json({
            res: undefined,
            error: 'User already exists',
        })
    }
)
