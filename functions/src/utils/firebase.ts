import * as admin from 'firebase-admin'
import * as cors from 'cors'

export const firebase = admin.initializeApp()
export const corsInstance = cors({ origin: true })

export default firebase